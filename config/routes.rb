# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :slack, defaults: { format: 'json' } do
    post 'events', to: 'translator#events'
  end
end
