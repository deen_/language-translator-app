# frozen_string_literal: true

class Translator
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(channel, event_ts, text)
    @channel = channel
    @event_ts = event_ts
    @text = text

    translate_text
  end

  def lang
    @text.split("\n")[0].delete("\u00A0")
  end

  def to_translate
    @text.slice(/\[(.*?)\]/, 1)
  end

  def translate_text
    translator = DeeplService.new(to_translate, lang)
    response = translator.translate

    return unless response

    resp = JSON.parse(response&.body || "{}")
    @translated_text = resp["translations"][0]["text"]
    @src_lang = resp["translations"][0]["detected_source_language"]

    display_translation
    save_to_sheet
  end

  def display_translation
    slack = SlackService.new(@channel, @event_ts, @translated_text)
    slack.post_message
  end

  def save_to_sheet
    sheet = SheetsService.new({
                                src_lang:        @src_lang,
                                dest_lang:       lang,
                                to_translate:    to_translate,
                                translated_text: @translated_text
                              })
    sheet.save_translation
  end
end
