# frozen_string_literal: true

class SheetsService
  def initialize(data)
    session = GoogleDrive::Session.from_service_account_key(
      "slack-auto-translator-326202-81c18982d37b.json"
    )
    @spreadsheet = session.spreadsheet_by_title("Slack App Translations")

    @src_lang = data[:src_lang]
    @dest_lang = data[:dest_lang]
    @to_translate = data[:to_translate]
    @translated_text = data[:translated_text]

    @sheet_title = "#{@src_lang}-#{@dest_lang}"
  end

  def save_translation
    worksheet = @spreadsheet.worksheet_by_title(@sheet_title)
    worksheet = new_worksheet if worksheet.nil?

    # inserting row
    worksheet.insert_rows(worksheet.num_rows + 1,
                          [
                            [@to_translate, @translated_text]
                          ])

    worksheet.save
  end

  def new_worksheet
    ws = @spreadsheet.add_worksheet(@sheet_title, index: nil)
    ws.insert_rows(ws.num_rows + 1,
                   [
                     ["TEXT", "TRANSLATION"]
                   ])
    ws.save
    ws
  end
end
