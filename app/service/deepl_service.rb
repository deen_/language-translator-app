# frozen_string_literal: true

class DeeplService
  def initialize(text, lang)
    @text = URI.parse URI.encode_www_form_component(text)
    @lang = lang
  end

  def translate
    HTTParty.get(
      "#{ENV['DEEPL_HOST']}/translate?" \
      "auth_key=#{ENV['DEEPL_AUTH_KEY']}&text=#{@text}&target_lang=#{@lang}"
    )
  end
end
