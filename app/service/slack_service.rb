# frozen_string_literal: true

class SlackService
  def initialize(channel, event_ts, translated_text)
    @channel = channel
    @event_ts = event_ts
    @translated_text = translated_text
  end

  def post_message
    HTTParty.post(
      "https://slack.com/api/chat.postMessage",
      query:   {
        channel:   @channel,
        thread_ts: @event_ts,
        text:      "[#{@translated_text}]"
      },
      headers: { Authorization: "Bearer #{ENV['SLACK_TOKEN']}" }
    )
  end
end
