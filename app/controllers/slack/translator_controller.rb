# frozen_string_literal: true

module Slack
  class TranslatorController < Slack::BaseController
    def events
      @result = { challenge: params[:challenge] }
      render json: @result, status: :ok

      return unless params[:type] != "url_verification"

      set_slack_vars
      Translator.perform_async(@channel, @event_ts, @text)

      # translate_text if params[:type] != "url_verification"
    end

    private

    def set_slack_vars
      @channel = params[:event][:channel]
      @event_ts = params[:event][:event_ts]

      @text = params[:event][:text]
      @text.slice! ENV["SLACK_APP_MENTION_ID"]
    end
  end
end
